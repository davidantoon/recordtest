import groovy.json.JsonBuilder
import groovy.json.JsonOutput

import java.text.SimpleDateFormat

/**
 * Created by yoavg on 05/07/2017.
 */
@Grab(group = 'commons-io', module = 'commons-io', version = '2.4')

    class Event {
    String eventId
    String date
    String eventType
    String eventClass
    String eventMethod
    String eventParams = ""
    String description
    String filename = ""
}


def parse(args) {
    if (args.length < 3) {
        println("args needs to be: fileName packageName sessionId")
        System.exit(1)
    }

    String filename = args[0]
    def packageName = args[1]
    File folder = new File(args[2])
    if (!folder.exists()) {
        folder.mkdir()
    }

    def filter = "HackLog--"+packageName
    def year = Calendar.getInstance().get(Calendar.YEAR)

    def eventId = 0

    List<Event> events = new ArrayList<>()
    List<String> files = new ArrayList<>()

    def file = new File(filename)
    file.eachLine { String line ->
        if (!line.contains(filter))
            return

        def event = new Event()
        event.eventId = eventId++

        def lineParts = line.split(" ")
        String dateStr =  year + "-" +lineParts[0]+ " " + lineParts[1]
        //def date = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr)
        def date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").parse(dateStr)


        event.date = JsonOutput.toJson(date)

        def payloadParts = lineParts[lineParts.length-1].split(",")

        event.eventClass = payloadParts[0]
        event.eventMethod = payloadParts[1].split("\\.")[1]
        List<String> params = new ArrayList<>()
        if (payloadParts.length > 2) {
            for (int i=2; i < payloadParts.length; i++) {
                params.add(payloadParts[i])
            }
            event.eventParams = params.join(",")
        }

        if (payloadParts[0] == "Activity") {
            event.eventType = "UI"
            event.description = lineParts[1]
            event.filename = ""
        }
        else if (payloadParts[0] == "BlockGuardOs") {
            event.eventType = "IO"
            event.description = event.eventMethod + ": " + params[0].substring(2)
            if (event.eventMethod == "open") {
                event.filename = params[0].substring(2)
                files.add(event.eventId+","+event.filename)
            }
        }
        else
            return

        events.add(event)
    }


    def jsonBuilder = new JsonBuilder()
    jsonBuilder {
        count(events.size())
        data events.collect({
            [

                        id: it.eventId,
                        date:  it.date,
                        type: it.eventType,
                        class: it.eventClass,
                        method: it.eventMethod,
                        params: it.eventParams,
                        description: it.description,
                        filename: it.filename

            ]
        })
    }

    def json = jsonBuilder.toPrettyString()
    println(json)

    if (files.size() > 0) {
        File filesList = new File(folder,"files")
        FileWriter writer = new FileWriter(filesList)
        for (String str: files) {
            writer.write(str+"\n")
        }
        writer.close()
    }



}

parse(args)