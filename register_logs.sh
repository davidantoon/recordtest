#!/bin/sh

yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 111; }
try() { "$@" || die "$* failed"; }

DEVICE_ID="$1"
LOG_PATH="$2"

adb -s $DEVICE_ID logcat -c

cmd="adb -s $DEVICE_ID logcat -s -v threadtime HackLog--com.checkpoint.hackaton.rocknroll:V"
eval $cmd | tee $LOG_PATH &>/dev/null &
LOGCAT_PID=$!
echo "$LOGCAT_PID"