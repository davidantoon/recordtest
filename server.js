const ws = require("nodejs-websocket")
const merge = require('mout/object/merge');
const spawn = require('child_process').spawn;
const express = require('express');
const Utils = require('./Utils').Utils;
const app = express();
const TAG = "Server";
const Splitter = require('stream-split');
const NALseparator = new Buffer([0, 0, 0, 1]);//NAL break
var fs = require('fs');


process.on("uncaughtException", function (err) {
    console.error(err);
});
var _Server = function (serverConf) {

    this.lockedDevices = [];
    this.options = {
        width: 720,
        height: 1280,
        fps: 60
    };


    this.startStreaming = function (socket) {
        if (socket.recorderSpawn != null) {
            socket.recorderSpawn.kill();
        }

        socket.recorderSpawn = Utils.createStreamer(socket);
        socket.readStream = socket.recorderSpawn.stdout.pipe(new Splitter(NALseparator));
        socket.readStream.on("data", function (data) {
            socket.send(Buffer.concat([NALseparator, data]), {binary: true}, function ack(error) {
            });
        });
    };

    this.new_client = function (socket) {
        var self = this;
        if (this.wss.clients == null || this.wss.clients == undefined) {
            this.wss.clients = [];
        }
        this.wss.clients.push(socket);
        socket.status = 0;
        socket.applicationPath = __dirname + "/apps/app-debug.apk";
        socket.applicationPathWrapped = __dirname + "/apps/app-debug-wrapped.apk";
        socket.appPackageName = "com.checkpoint.hackaton.rocknroll";
        allocateDevice(socket, function () {
            console.log(TAG, "allocateDevice", "device allocated successfully: " +
                socket.deviceId + " (" + socket.width + "x" + socket.height + ")");

            socket.userEvents = "";
            socket.status = 1;
            socket.recordId = new Date().getTime();
            socket.clickedInterval = new Date().getTime();

            socket.send(JSON.stringify({
                action: "init",
                width: 720,
                height: 1280
            }));
        });

        socket.on("text", function (data) {
            if (socket.status == 0) {
                // initializing...
                return;
            }
            var action = data.split(' ')[0];

            if (action == "REQUESTSTREAM") {
                self.startStreaming(socket);
            } else if (action == "STOPSTREAM") {
                self.readStream.pause();
            } else {
                try {
                    var json = JSON.parse(data);
                    if (json.action == "tap") {
                        if (socket.status == 1) {
                            // installing application
                            return;
                        }
                        var tapX = parseInt(json.x * parseInt(socket.width));
                        var tapY = parseInt(json.y * parseInt(socket.height));
                        Utils.tapOnDevice(socket, tapX, tapY);

                    } else if (json.action == "start_record") {
                        setTimeout(function () {
                            Utils.registerLogStreamer(socket, function (logSpawnId) {
                                socket.logsSpawn = logSpawnId;

                                Utils.runApplication(socket, function () {
                                    socket.status = 2;
                                    socket.clickedInterval = new Date().getTime();
                                })
                            });
                        }, 2000);
                    } else if (json.action == "stop_record") {
                        if (socket.status == 1) {
                            // installing application
                            return;
                        }
                        socket.status = 1;
                        // socket.readStream.pause();
                        // socket.recorderSpawn.kill();
                        var events = "#!/bin/sh\n\nDEVICE_ID=\"" + socket.deviceId + "\"\n";

                        events += "adb -s $DEVICE_ID uninstall " + socket.appPackageName + ";\n";
                        events += "adb -s $DEVICE_ID install " + socket.applicationPathWrapped + ";\n";
                        events += "adb -s $DEVICE_ID shell monkey -p " + socket.appPackageName + " 1;\n";
                        events += socket.userEvents;
                        events += "echo \"done\"";
                        // events += "adb -s $DEVICE_ID uninstall " + socket.appPackageName + ";\n";


                        socket.step2 = null;
                        socket.send(JSON.stringify({action: "server_event", data: "switch_to_wrapping_progress"}));
                        Utils.writeFile(__dirname + '/' + socket.recordId + '/recorded_test' + '.sh', events, function (err) {
                            Utils.getFileFromDevice(socket, "/files/file.txt", function (data) {
                                socket.unwrappedDeviceLogs = data;
                                Utils.readLogs(socket, function () {
                                    if (socket.step2 == null) {
                                        socket.step2 = 0;
                                        startSecondStep(socket);
                                    }
                                });
                            });
                        });
                    } else {
                        console.log(TAG, "socket_message", "unsupported operation", action);
                    }
                } catch (e) {
                }
            }
        });

        socket.on('close', function () {
            socket.readStream.pause();
            socket.recorderSpawn.kill();
            for (var i = 0; i < this.lockedDevices.length; i++) {
                if (this.lockedDevices[i] == socket.deviceId) {
                    this.lockedDevices.splice(i, 1);
                    break;
                }
            }
            for (var i = 0; i < this.wss.clients.length; i++) {
                if (this.wss.clients[i].id == socket.id) {
                    this.wss.clients.splice(i, 1);
                    break;
                }
            }
            console.log('stopping client interval');
        }.bind(this));
    };

    this.run = function () {

        this.wss = ws.createServer(function (conn) {
            this.new_client(conn);
        }.bind(this)).listen(serverConf.socketPort);

        app.get('/', function (req, res) {
            res.sendFile(__dirname + '/index.html');
        });
        app.get('/js/http-live-player.js', function (req, res) {
            res.sendFile(__dirname + '/js/http-live-player.js');
        });

        app.get('/js/jquery-3.2.1.min.js', function (req, res) {
            res.sendFile(__dirname + '/js/jquery-3.2.1.min.js');
        });

        app.get('/js/diff2html.min.js', function (req, res) {
            res.sendFile(__dirname + '/js/diff2html.min.js');
        });

        app.get('/css/diff2html.min.css', function (req, res) {
            res.sendFile(__dirname + '/css/diff2html.min.css');
        });

        app.listen(serverConf.serverPort, function () {
            console.log('Example app listening on port ' + serverConf.serverPort)
        });

        this.new_client = this.new_client.bind(this);
    }
};


var s = new _Server({socketPort: 8081, serverPort: 8080});
s.run();


function allocateDevice(socket, callback) {

    const getDeviceScreenSize = function () {
        console.log(TAG, "allocateDevice", "get device screen size");
        Utils.getDeviceSize(socket.deviceId, function (res, error) {
            if (error != null) {
                throw Error("Failed to get device screen size " + socket.deviceId, error);
            }
            socket.width = res.width;
            socket.height = res.height;

            console.log(TAG, "allocateDevice", "installing the application");
            Utils.reinstallApp(socket, callback);

        })
    };

    console.log(TAG, "allocateDevice", "get devices list");
    Utils.getDevices(function (devices, error) {
        if (error != null) {
            throw Error("Failed to allocate devices", error);
        }
        for (var j = 0; j < devices.length; j++) {
            var locked = false;
            for (var i = 0; i < s.lockedDevices.length; i++) {
                if (s.lockedDevices[i] == devices[j]) {
                    locked = true;
                }
            }
            if (!locked) {
                socket.deviceId = devices[j];
                s.lockedDevices.push(socket.deviceId);
                getDeviceScreenSize(callback);
                return;
            }
        }
        throw Error("Failed to allocate devices");
    });
}


function startSecondStep(socket) {
    socket.send(JSON.stringify({action: "wrapping_logs", data: "Wrapping application"}));
    setTimeout(function () {
        socket.send(JSON.stringify({action: "wrapping_logs", data: "Wrapping application"}));
        setTimeout(function () {
            socket.send(JSON.stringify({action: "wrapping_logs", data: "Wrapping application."}));
            setTimeout(function () {
                socket.send(JSON.stringify({action: "wrapping_logs", data: "Wrapping application.."}));
                setTimeout(function () {
                    socket.send(JSON.stringify({action: "wrapping_logs", data: "Wrapping application..."}));

                    setTimeout(function () {
                        socket.send(JSON.stringify({action: "wrapping_logs", data: "Wrapping application."}));
                        setTimeout(function () {
                            socket.send(JSON.stringify({action: "wrapping_logs", data: "Wrapping application.."}));
                            setTimeout(function () {
                                socket.send(JSON.stringify({action: "wrapping_logs", data: "Wrapping application..."}));

                                setTimeout(function () {
                                    socket.send(JSON.stringify({
                                        action: "wrapping_logs",
                                        data: "Prepare device for testing"
                                    }));
                                    setTimeout(function () {
                                        socket.send(JSON.stringify({
                                            action: "wrapping_logs",
                                            data: "Installing " + socket.appPackageName
                                        }));
                                        setTimeout(function () {

                                            setTimeout(function () {
                                                socket.send(JSON.stringify({
                                                    action: "server_event",
                                                    data: "switch_to_device_screen"
                                                }));
                                            }, 3000);
                                            Utils.registerLogWrappingStreamer(socket, function () {
                                                // 4. start recorded test with wrapped app

                                                Utils.startTestRecord(socket, function () {

                                                    // 5. pull logs after test completed
                                                    Utils.readWrappedLogs(socket, function () {
                                                        // Utils.getFileFromDevice(socket, "/files/file.txt", function (data) {
                                                        //     socket.wrappedDeviceLogs = data;

                                                        // fs.readFile(__dirname + "/" + socket.recordId + "/unwrapped_logs.json", 'utf8', function (err, undata) {
                                                        //     var unwrapped = JSON.parse(undata);
                                                        //     fs.readFile(__dirname + "/" + socket.recordId + "/wrapped_logs.json", 'utf8', function (err, wrpp) {
                                                        //         var wrapped = JSON.parse(wrpp);

                                                        // 7. send server_event to show compare logs
                                                        socket.send(JSON.stringify({
                                                            action: "server_event",
                                                            data: "switch_to_compare_logs"
                                                        }));
                                                        // dec: socket.unwrappedDeviceLogs,
                                                        // enc: new Buffer(socket.wrappedDeviceLogs).toString('base64')
                                                        // unwrapped: JSON.stringify(unwrapped),
                                                        // wrapped: JSON.stringify(wrapped)
                                                        // });
                                                        // });
                                                        // });
                                                    })
                                                });
                                            });
                                        }, 4000);
                                    }, 4000);
                                }, 1000);
                            }, 1000);
                        }, 1000);
                    }, 1000);
                }, 1000);
            }, 1000);
        }, 1000);
    }, 1000);

}