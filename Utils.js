const TAG = "Utils";
const Utils = {};
const spawn = require('child_process').spawn;
const mkdirp = require('mkdirp');
const getDirName = require('path').dirname;
var fs = require('fs');

const utf8ArrayToStr = function (array) {
    var out, i, len, c;
    var char2, char3;

    out = "";
    len = array.length;
    i = 0;
    while (i < len) {
        c = array[i++];
        switch (c >> 4) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                // 0xxxxxxx
                out += String.fromCharCode(c);
                break;
            case 12:
            case 13:
                // 110x xxxx   10xx xxxx
                char2 = array[i++];
                out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                break;
            case 14:
                // 1110 xxxx  10xx xxxx  10xx xxxx
                char2 = array[i++];
                char3 = array[i++];
                out += String.fromCharCode(((c & 0x0F) << 12) |
                    ((char2 & 0x3F) << 6) |
                    ((char3 & 0x3F) << 0));
                break;
        }
    }

    return out;

};

const runBash = function (command, args, callback) {
    var sw = spawn(command, args);
    sw.stdout.on("data", function (data) {
        callback(utf8ArrayToStr(data));
        sw.kill();
    });
    sw.stderr.on("data", function (data) {
        callback(null, utf8ArrayToStr(data));
        sw.kill();
    });
};
const adb = function (deviceId, command, callback) {
    if (deviceId == null) {
        runBash("adb", command.trim().split(" "), callback);
    } else {
        runBash("adb", ("-s " + deviceId + " " + command.trim()).split(" "), callback);
    }
};
const adbArray = function (command, callback) {
    runBash("adb", command, callback);
};


Utils.getDevices = function (callback) {
    console.log(TAG, "getDevices");
    adb(null, "devices -l", function (res, error) {
        if (error != null) {
            callback(null, error);
            return;
        }
        res = res.substring(res.indexOf("\n") + 1);
        res = res.substring(0, res.length - 2);
        var devices = res.split("\n");
        var deviceIds = [];
        for (var i = 0; i < devices.length; i++) {
            var m = /usb:([^\s]*)/;
            var deviceId = m.exec(devices[i])[0];
            deviceIds.push(deviceId);
        }
        callback(deviceIds);
    });
};

Utils.getDeviceSize = function (deviceId, callback) {
    console.log(TAG, "getDeviceSize", deviceId);
    adb(deviceId, "shell wm size", function (res, error) {
        if (error != null) {
            callback(null, error);
            return;
        }

        var m = /[0-9]([^\ ]+)x([^\ ]+)/;
        callback({
            width: parseInt(m.exec(res)[0].split("x")[0]),
            height: parseInt(m.exec(res)[0].split("x")[1])
        });
    });
};

Utils.tapOnDevice = function (socket, tapX, tapY) {
    console.log(TAG, "tapOnDevice", "tap", tapX, tapY, "(device: " + socket.deviceId + ")");
    var duration = (new Date().getTime() - socket.clickedInterval) / 1000;
    socket.clickedInterval = new Date().getTime();
    socket.userEvents += "sleep " + duration + "; " + "adb -s $DEVICE_ID " + "shell input tap " + tapX + " " + tapY + ";\n";
    adb(socket.deviceId, "shell input tap " + tapX + " " + tapY);
};

Utils.createStreamer = function (socket) {
    return spawn('adb', ["-s", socket.deviceId, "shell", "screenrecord", "--size", 720 + "x" + 1280,
        "--bit-rate", "921600", "--output-format=h264", "-"]);

};

Utils.registerLogStreamer = function (socket, callback) {

    var path = __dirname + "/" + socket.recordId + "/device_logs.txt";
    mkdirp(getDirName(path), function (err) {
        if (err) return callback(err);
        runBash("sh", [__dirname + "/register_logs.sh", socket.deviceId, path], function (s, e) {
            if (e != null) {
                console.log(TAG, "createLogStreamer", e);
                callback(e);
            } else {
                console.log(TAG, "createLogStreamer", s);
                callback(s);
            }
        });
    });
};
Utils.registerLogWrappingStreamer = function (socket, callback) {

    var path = __dirname + "/" + socket.recordId + "/device_logs_wrapped.txt";
    mkdirp(getDirName(path), function (err) {
        if (err) return callback(err);
        runBash("sh", [__dirname + "/register_logs.sh", socket.deviceId, path], function (s, e) {
            if (e != null) {
                console.log(TAG, "createLogStreamer", e);
                callback(e);
            } else {
                console.log(TAG, "createLogStreamer", s);
                callback(s);
            }
        });
    });
};

Utils.reinstallApp = function (socket, callback) {

    adb(socket.deviceId, "uninstall " + socket.appPackageName, function (success, error) {
        console.log(TAG, "reinstallApp", "uninstalling by package id", success);
        adb(socket.deviceId, "install " + socket.applicationPath, function (success, error) {
            if (error != null && error != "Success\n") {
                throw Error(error);
            }
            console.log(TAG, "reinstallApp", "apk installed", error);
            callback();
        });
    });

};

Utils.reinstallWrappedApp = function (socket, callback) {

    adb(socket.deviceId, "uninstall " + socket.appPackageName, function (success, error) {
        console.log(TAG, "reinstallApp", "uninstalling by package id", success);
        adb(socket.deviceId, "install " + socket.applicationPathWrapped, function (success, error) {
            if (error != null && error != "Success\n") {
                throw Error(error);
            }
            console.log(TAG, "reinstallApp", "wrapped apk installed", error);
            callback();
        });
    });

};

Utils.runApplication = function (socket, callback) {
    adb(socket.deviceId, "shell monkey -p " + socket.appPackageName + " 1", function (success, error) {
        console.log(TAG, "runApplication", socket.appPackageName);
        console.log(TAG, "runApplication", success);
        console.log(TAG, "runApplication", error);
        callback();
    });
};

Utils.writeFile = function (path, contents, callback) {
    mkdirp(getDirName(path), function (err) {
        if (err) return callback(err);
        fs.writeFile(path, contents, callback);
    });
};

Utils.getFileFromDevice = function (socket, relativePath, callback) {
    runBash("sh", [__dirname + "/pull_logs.sh", socket.deviceId, socket.appPackageName, relativePath], function (s, e) {
        if (e != null) {
            callback(e);
        } else {
            callback(s);
        }
    });
};
Utils.getFileFromDevicePublic = function (socket, path, callback) {
    runBash("sh", [__dirname + "/pull_public_logs.sh", socket.deviceId, path], function (s, e) {
        if (e != null) {
            callback(e);
        } else {
            callback(s);
        }
    });
};

Utils.readLogs = function (socket, callback) {
    runBash("sh", ["kill", parseInt(socket.logsSpawn)], function () {

        runBash("sh", [__dirname + "/read_logs.sh", __dirname + "/parser.groovy",
            __dirname + "/" + socket.recordId + "/device_logs.txt", socket.appPackageName, __dirname + "/" + socket.recordId + "/unwrapped_logs.json"], function (s, e) {
            if (e != null) {
                callback(e);
            } else {
                callback(s);
            }
        });
    });
};

Utils.readWrappedLogs = function (socket, callback) {
    runBash("sh", ["kill", parseInt(socket.logsSpawn)], function () {
        runBash("sh", [__dirname + "/read_logs.sh", __dirname + "/parser.groovy",
            __dirname + "/" + socket.recordId + "/device_logs_wrapped.txt", socket.appPackageName, __dirname + "/" + socket.recordId + "/wrapped_logs.json"], function (s, e) {
            if (e != null) {
                callback(e);
            } else {
                callback(s);
            }
        });
    });
};

Utils.startTestRecord = function (socket, callback) {

    var sw = spawn("sh", [__dirname + "/" + socket.recordId + "/recorded_test.sh"]);
    sw.stdout.on("data", function (data) {
        data = utf8ArrayToStr(data);
        console.log("data", data);
        if (data.indexOf("done") != -1) {
            callback();
        }
    });
    sw.stderr.on("data", function (data) {
        data = utf8ArrayToStr(data);
        console.log("data", data);
        if (data.indexOf("done") != -1) {
            callback();
        }
    });
};

exports.Utils = Utils;