#!/bin/sh

# from http://stackoverflow.com/a/25515370/1233652
yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 111; }
try() { "$@" || die "$* failed"; }

# clean the buffer
adb logcat -c

STATE="$1"

#cmd='adb logcat -s -v threadtime *:*'
cmd="adb logcat -s -v threadtime HackLog||com.checkpoint.hackaton.rocknroll:V"
input="/tmp/events.txt"

if [ "$STATE" = "--unwrapped-start" ]; then
	adb install /tmp/unwrapped.apk
	eval $cmd | tee /tmp/trace_unwrapped.txt &>/dev/null &
	LOGCAT_PID=$!
	echo "$LOGCAT_PID" > /tmp/unwrapped_logcat_pid.txt
	adb shell monkey -p com.checkpoint.hackaton.rocknroll 1
elif [ "$STATE" = "--unwrapped-stop" ]; then
	# kill the logcat process
	PID=`cat /tmp/unwrapped_logcat_pid.txt`
	kill $PID
	groovy /tmp/trace_unwrapped.txt com.checkpoint.hackaton.rocknroll 1
	# TODO: adb pull the files
	adb shell pm uninstall com.checkpoint.hackaton.rocknroll
fi 

LOGCAT_PID=""

if [ "$STATE" = "--wrapped-start" ]; then
	adb install /tmp/wrapped.apk
	eval $cmd | tee /tmp/trace_wrapped.txt &>/dev/null &
	LOGCAT_PID=$!
	echo "$LOGCAT_PID" > /tmp/wrapped_logcat_pid.txt
	adb shell monkey -p com.checkpoint.hackaton.rocknroll 1
	
	# running the events
	while IFS= read -r var
	do
		echo "$var"
		eval $var
	done < "$input"
	STATE="--wrapped-stop"
fi

if [ "$STATE" = "--wrapped-stop" ]; then
	PID=`cat /tmp/wrapped_logcat_pid.txt`
	kill $PID
	groovy parser.groovy /tmp/trace_unwrapped.txt com.checkpoint.hackaton.rocknroll 2
	# adb pull the files
	adb shell pm uninstall com.checkpoint.hackaton.rocknroll
fi


#adb shell am start -n com.checkpoint.hackaton.rocknroll/com.package.name.ActivityName